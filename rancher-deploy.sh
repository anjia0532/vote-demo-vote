#!/bin/bash
#
# MODIFIED FROM: https://github.com/adamkdean/rancher-deploy-script


##########
# these values should be set from .gitlab-ci.yml - was set here for initial testing
# d1-dev
#RANCHER_ACCESS_KEY="0FC10CD4E0E71CBE5E08"
#RANCHER_SECRET_KEY="JrFeFVFsWiMZsPYpd7Pvkb4iWGQ6hE8TPV4zN8B9"
# d1-int
#RANCHER_ACCESS_KEY="77C75B0C6FB735493B79"
#RANCHER_SECRET_KEY="vMmQS5NKmam3xNMsaUg5snBydFCpFNp8kQMcoYir"

# d2-dev
#RANCHER_ACCESS_KEY="CBBDEA01BCE247D40253"
#RANCHER_SECRET_KEY="D7btp1LdFP91AFpXS1yG5fzdtMBkc5JsnAHnuuAN"
# d2-int
#RANCHER_ACCESS_KEY="E54FF0D5A7C01E62F7EA"
#RANCHER_SECRET_KEY="xb1wW8X36mhibhFnykkUhjxenH51A7wWsuCbgFk5"

#RANCHER_HOST="rancher-dr.lightcloudit.com:8080"
#RANCHER_HOST="rancher.lightcloudit.com"
##########

if [ $1 ]; then
  REF_NAME="$1"
else
  REF_NAME="${CI_BUILD_REF_NAME}"
fi

SERVICE_NAME="$(echo ${CI_PROJECT_NAME}-app)"
CURL="curl --user $RANCHER_ACCESS_KEY:$RANCHER_SECRET_KEY"
RANCHER_PROTO="http"
RANCHER_LOC="$RANCHER_PROTO://$RANCHER_HOST"

echo "service_name curl rancher_proto rancher_loc - ${SERVICE_NAME}   ${CURL}    ${RANCHER_PROTO}    ${RANCHER_LOC}" 
echo "rancher variables  ${RANCHER_HOST}   ${RANCHER_ENV}    ${RANCHER_ACCESS_KEY}   ${RANCHER_SECRET_KEY}"

# reference: https://forums.rancher.com/t/upgrading-services-via-api/1629/4
echo "Refresh the catalog templates"
CATALOG_URL="$RANCHER_LOC/v1-catalog/templates?action=refresh"
CATALOG_REFRESH=$($CURL -H "Content-Type: application/json" -H 'Accept: application/json' -X POST $CATALOG_URL)

echo "Need to add a check here to make the script smarter on first-time deployments - I suspect that the string is empty - need to detect if a service is there or not"
SERVICE_URL="$RANCHER_LOC/v1/services?name=$SERVICE_NAME"
SERVICE_JSON=$($CURL $SERVICE_URL)

STATE=$(echo $SERVICE_JSON | jq -r '.data[0].state' | sed -e 's/^"//'  -e 's/"$//')
SELF=$(echo $SERVICE_JSON | jq -r '.data[0].links.self' | sed -e 's/^"//'  -e 's/"$//')

if [[ $STATE != "active" ]]; then
  echo "[ERROR] Service $SERVICE_NAME state is '$STATE', must be set to 'active'"
  exit 1
fi

UPGRADE_TIMEOUT=120
UPGRADE_BATCH_SIZE=1
UPGRADE_INTERVAL_MILLIS=2000
UPGRADE_START_FIRST="true"
UPGRADE_URL=$(echo $SERVICE_JSON | jq -r '.data[0].actions .upgrade' | sed -e 's/^"//'  -e 's/"$//')
UPGRADE_SLC=$(echo $SERVICE_JSON | jq -r '.data[0].secondaryLaunchConfigs')

CURRENT_LC=$(echo $SERVICE_JSON | jq -r '.data[0].launchConfig' )
CURRENTSVCHASH=$( echo $CURRENT_LC | jq -r '.labels["io.rancher.service.hash"]' | awk -F: '{print $NF}' )
CURRENTIMG=$( echo $CURRENT_LC | jq -r .imageUuid | awk -F: '{print $NF}' )
#"imageUuid": "docker:registry.gitlab.lightcloudit.com/ael/vote-demo-vote:jliu-rancher_v1.0.2_72_181be427",
CURRENTVER=$( echo $CURRENT_LC | jq -r .version | awk -F: '{print $NF}' )

if [[ $REF_NAME == "stg" ]]; then
  UPGRADE_LC=$(echo $SERVICE_JSON | jq -r '.data[0].launchConfig' | sed -e "s/${CURRENTIMG}/${APP_VERSION}/" | sed -e 's/\"0\"/\"1\"/' )
elif  [[ $REF_NAME == "prd" ]]; then
  UPGRADE_LC=$(echo $SERVICE_JSON | jq -r '.data[0].launchConfig' | sed -e "s/${CURRENTIMG}/${APP_VERSION}/" | sed -e 's/\"0\"/\"1\"/' )
else
  UPGRADE_LC=$(echo $SERVICE_JSON | jq -r '.data[0].launchConfig' | sed -e "s/${CURRENTIMG}/${CI_BUILD_REF_NAME}_${APP_VERSION}_${COMMITCOUNT}_${COMMITHASH}/" | sed -e 's/\"0\"/\"1\"/' )
fi


BODY="{ \"inServiceStrategy\": { \
  \"batchSize\": $UPGRADE_BATCH_SIZE, \
  \"intervalMillis\": $UPGRADE_INTERVAL_MILLIS, \
  \"startFirst\": $UPGRADE_START_FIRST, \
  \"launchConfig\": $UPGRADE_LC, \
  \"secondaryLaunchConfigs\": $UPGRADE_SLC } }"

echo "##############"
echo UPGRADE_URL
echo $UPGRADE_URL 
#echo UPGRADE_LC
echo $UPGRADE_LC | jq .
#echo UPGRADE_SLC
#echo $UPGRADE_SLC | jq .
#echo BODY
echo $BODY | jq .
echo "##############"

echo "[Upgrading $SERVICE_NAME]"
RESPONSE=$($CURL -H "Content-Type: application/json" -X POST -d "$BODY" $UPGRADE_URL)
RESPONSE=$($CURL -H "Content-Type: application/json" -X POST -d "$BODY" $UPGRADE_URL)

echo "[Waiting for service $SERVICE_NAME to upgrade]"
wait4upgrade() {
  CNT=0
  STATE=""
  until [[ $STATE == "upgraded" ]]; do
    STATE=$($CURL --silent $SELF | jq -r '.state' | sed -e 's/^"//'  -e 's/"$//')
    echo "Service state: $STATE"
    if [ $((CNT++)) -gt $UPGRADE_TIMEOUT ]; then
        echo "Upgrade timed out, state: $STATE"
        exit 1
    else
        sleep 1
    fi
  done
}
wait4upgrade


FINISH_UPGRADE_URL=$($CURL $SELF | jq -r '.actions.finishupgrade' | sed -e 's/^"//'  -e 's/"$//')
echo "[Confirming upgrade via $FINISH_UPGRADE_URL]"
RESPONSE=$($CURL -X POST $FINISH_UPGRADE_URL)

echo "[Waiting for service $SERVICE_NAME to finish upgrade]"
wait4finishupgrade() {
  CNT=0
  STATE=""
  until [[ $STATE == "active" ]]; do
    STATE=$($CURL --silent $SELF | jq -r '.state' | sed -e 's/^"//'  -e 's/"$//')
    echo "Service state: $STATE"
    if [ $((CNT++)) -gt $UPGRADE_TIMEOUT ]; then
        echo "Confirm Upgrade timed out, state: $STATE"
        exit 1
    else
        sleep 1
    fi
  done
}
wait4finishupgrade

echo "[ALL DONE]"
exit 0
