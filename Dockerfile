# Using official python runtime base image
FROM python:2.7-alpine

# Set the application directory
WORKDIR /app

# Install our requirements.txt
COPY requirements.txt /app/requirements.txt
# 8/23/2016 - issues with dl-cdn.alpinelinux.org # https://github.com/gliderlabs/docker-alpine/issues/209 # https://status.fastly.com/incidents/0523znjnppc4
# RUN sed -e 's/cdn/4/g' /etc/apk/repositories > /etc/apk/new && mv /etc/apk/new /etc/apk/repositories
RUN pip install -r requirements.txt && apk add --update bash curl jq

# Copy our code from the current folder to /app inside the container
COPY . /app

# Make port 80 available for links and/or publish
EXPOSE 80

#add entrypoint and start up scripts
COPY .docker/entrypoint.sh /usr/local/bin

#entrypoint script to set env vars when linking containers for dev
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

# Define our command to be run when launching the container
CMD ["gunicorn", "app:app", "-b", "0.0.0.0:80", "--log-file", "-", "--access-logfile", "-", "--workers", "4", "--keep-alive", "0"]
